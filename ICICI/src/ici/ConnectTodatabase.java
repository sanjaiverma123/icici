package ici;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;


public class ConnectTodatabase {

	
	
	 public static Connection getConnection()
	    {
	        Connection con = null;
	        InitialContext ctx = null;
	        try
	        {
	            ctx = new InitialContext();
	            DataSource ds = (DataSource)ctx.lookup("java:ldaDS");
	            con = ds.getConnection();
	        }
	        catch(NamingException ne)
	        {
	            System.out.println((new StringBuilder("Exception 1")).append(ne.getMessage()).toString());
	        }
	        catch(SQLException se)
	        {
	            System.out.println((new StringBuilder("Exception 2")).append(se.getMessage()).toString());
	        }
	        catch(Exception se)
	        {
	            System.out.println((new StringBuilder("Exception 3")).append(se.getMessage()).toString());
	        }
	        return con;
	    }
}
