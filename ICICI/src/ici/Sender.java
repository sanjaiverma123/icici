// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   Sender.java

package ici;

import java.io.*;
import java.net.*;

public class Sender
{

    public Sender(String server, String username, String password, String mobile, String sendername, String message)
    {
        this.username = username;
        this.password = password;
        this.message = message;
        this.mobile = mobile;
        this.sendername = sendername;
    }

    public String submitMessage()
    {
        String dataFromUrl = "";
        String dataBuffer = "";
        try
        {
            URL sendUrl = new URL("http://priority.muzztech.in/sms_api/sendsms.php");
            HttpURLConnection httpConnection = (HttpURLConnection)sendUrl.openConnection();
            httpConnection.setRequestMethod("POST");
            httpConnection.setDoInput(true);
            httpConnection.setDoOutput(true);
            httpConnection.setUseCaches(false);
            DataOutputStream dataStreamToServer = new DataOutputStream(httpConnection.getOutputStream());
            dataStreamToServer.writeBytes((new StringBuilder("username=")).append(URLEncoder.encode(username, "UTF-8")).append("&password=").append(URLEncoder.encode(password, "UTF-8")).append("&mobile=").append(URLEncoder.encode(mobile, "UTF-8")).append("&sendername=").append(URLEncoder.encode(sendername, "UTF-8")).append("&message=").append(URLEncoder.encode(message, "UTF-8")).toString());
            dataStreamToServer.flush();
            dataStreamToServer.close();
            BufferedReader dataStreamFromUrl = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
            while((dataBuffer = dataStreamFromUrl.readLine()) != null) 
                dataFromUrl = (new StringBuilder(String.valueOf(dataFromUrl))).append(dataBuffer).toString();
            dataStreamFromUrl.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return dataFromUrl;
    }

    private static StringBuffer convertToUnicode(String regText)
    {
        char chars[] = regText.toCharArray();
        StringBuffer hexString = new StringBuffer();
        for(int i = 0; i < chars.length; i++)
        {
            String iniHexString = Integer.toHexString(chars[i]);
            if(iniHexString.length() == 1)
                iniHexString = (new StringBuilder("000")).append(iniHexString).toString();
            else
            if(iniHexString.length() == 2)
                iniHexString = (new StringBuilder("00")).append(iniHexString).toString();
            else
            if(iniHexString.length() == 3)
            {
                iniHexString = (new StringBuilder("0")).append(iniHexString).toString();
                hexString.append(iniHexString);
            }
        }

        System.out.println(hexString);
        return hexString;
    }

    String message;
    String type;
    String sendername;
    String mobile;
    String dlr;
    String server;
    int port;
    String destination;
    String username;
    String password;
    String source;
}
